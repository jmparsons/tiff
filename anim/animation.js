(function (lib, img, cjs) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
  width: 300,
  height: 250,
  fps: 15,
  color: "#F4F4F4",
  manifest: [
    {src:"images/BAR.png", id:"BAR"},
    {src:"images/BGOW.png", id:"BGOW"},
    {src:"images/BGTB.png", id:"BGTB"},
    {src:"images/F1.png", id:"F1"},
    {src:"images/F2.jpg", id:"F2"},
    {src:"images/F3COPY.png", id:"F3COPY"},
    {src:"images/F3IMG.jpg", id:"F3IMG"},
    {src:"images/LOGO.png", id:"LOGO"},
    {src:"images/STROKE.png", id:"STROKE"}
  ]
};



// symbols:



(lib.BAR = function() {
  this.initialize(img.BAR);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.BGOW = function() {
  this.initialize(img.BGOW);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.BGTB = function() {
  this.initialize(img.BGTB);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.F1 = function() {
  this.initialize(img.F1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.F2 = function() {
  this.initialize(img.F2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.F3COPY = function() {
  this.initialize(img.F3COPY);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.F3IMG = function() {
  this.initialize(img.F3IMG);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.LOGO = function() {
  this.initialize(img.LOGO);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.STROKE = function() {
  this.initialize(img.STROKE);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.MCStroke = function() {
  this.initialize();

  // Layer 1
  this.instance = new lib.LOGO();
  this.instance.setTransform(-150,-125);

  this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(-150,-125,300,250);
p.frameBounds = [rect];


(lib.MCF3IMG = function() {
  this.initialize();

  // Layer 1
  this.instance = new lib.F3IMG();

  this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,300,250);
p.frameBounds = [rect];


(lib.MCF3COPY = function() {
  this.initialize();

  // Layer 1
  this.instance = new lib.F3COPY();

  this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,300,250);
p.frameBounds = [rect];


(lib.MCF2 = function() {
  this.initialize();

  // Layer 1
  this.instance = new lib.F2();

  this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,300,250);
p.frameBounds = [rect];


(lib.MCF1 = function() {
  this.initialize();

  // Layer 1
  this.instance = new lib.F1();

  this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,300,250);
p.frameBounds = [rect];


// stage content:
(lib._2016_SPR130_BRI_TIFFSET_SDRBAND_GEM_CAEn_DSK_ANI_HTML5_300x250 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; } this.initialize(mode,startPosition,loop,{});

  // STROKE
  this.instance = new lib.STROKE();

  this.timeline.addTween(cjs.Tween.get(this.instance).wait(226));

  // LOGO
  this.instance_1 = new lib.MCStroke();
  this.instance_1.setTransform(150,125);
  this.instance_1.alpha = 0;
  this.instance_1._off = true;

  this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(147).to({_off:false},0).to({alpha:1},15).wait(64));

  // F3 (COPY)
  this.instance_2 = new lib.MCF3COPY();
  this.instance_2.setTransform(150,125,0.95,0.95,0,0,0,150,125);
  this.instance_2.alpha = 0;
  this.instance_2._off = true;

  this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(147).to({_off:false},0).to({scaleX:0.96,scaleY:0.96,x:150.1,alpha:1},15).to({scaleX:1,scaleY:1,x:150},63).wait(1));

  // F3 (IMG)
  this.instance_3 = new lib.MCF3IMG();
  this.instance_3.setTransform(150,300,1,1,0,0,0,150,300);
  this.instance_3.alpha = 0;
  this.instance_3._off = true;

  this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(147).to({_off:false},0).to({alpha:1},15).wait(64));

  // BAR
  this.instance_4 = new lib.BAR();

  this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(226));

  // F2
  this.instance_5 = new lib.MCF2();
  this.instance_5.setTransform(150,300,1,1,0,0,0,150,300);
  this.instance_5.alpha = 0;
  this.instance_5._off = true;

  this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(73).to({_off:false},0).to({alpha:1},15).wait(45).to({alpha:0.93},0).to({alpha:0},14).to({_off:true},1).wait(78));

  // F1
  this.instance_6 = new lib.MCF1();
  this.instance_6.setTransform(150,300,1,1,0,0,0,150,300);
  this.instance_6.alpha = 0;

  this.timeline.addTween(cjs.Tween.get(this.instance_6).to({alpha:1},14).wait(45).to({alpha:0.93},0).to({alpha:0},14).to({_off:true},1).wait(152));

  // BG (Tiffany Blue)
  this.instance_7 = new lib.BGTB();
  this.instance_7._off = true;

  this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(133).to({_off:false},0).wait(93));

  // BG (Off White)
  this.instance_8 = new lib.BGOW();

  this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(226));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(150,125,300,250);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;